﻿using UnityEngine;
using System.Collections;

public class cameraMover : MonoBehaviour {

	public float cameraDistOffset;
	private Camera mainCamera;
	public GameObject player;

	// Use this for initialization
	void Start () {
		mainCamera = GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update () {
		Vector3 playerInfo = player.transform.transform.position;
		mainCamera.transform.position = new Vector3(0, playerInfo.y + cameraDistOffset, playerInfo.z + 54);
	}
}
