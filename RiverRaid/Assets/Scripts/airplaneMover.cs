﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

[System.Serializable]
public class Boundary{
	public float xMin, xMax, zMin, zMax;
}

public class airplaneMover : MonoBehaviour {

	private Rigidbody rb;

	public Object cube;
	public Object cube2;

	public Object woodImage;
	public Object boatImage;

	public Transform shotSpawn;

	public float speed;
	public float tilt;

	public static float globalSpeed;

	public Boundary boundary;
	public Text gameOver;
	public Text finalScore;
	public Button restartGame;

	public static bool isGameOver;

	bool startPlay = true;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();

		gameOver.gameObject.SetActive(false);
		finalScore.gameObject.SetActive(false);
		restartGame.gameObject.SetActive (false);
		isGameOver = false;

		globalSpeed = speed;
	}

	void FixedUpdate(){

		float moveHorizontal = 0;

		if (Input.touchCount == 1)
		{
			var touch = Input.touches[0];
			if (touch.position.x < Screen.width/2)
			{
				moveHorizontal = -0.5f;
			}
			else if (touch.position.x > Screen.width/2)
			{
				moveHorizontal = 0.5f;
			}
		}

//		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0, 1);
		rb.velocity = movement * speed;

		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);

		if(startPlay){
			Instantiate (cube, new Vector3 (0, 0, 0), new Quaternion ());
			Instantiate (cube2, new Vector3 (0, 0,  62.5f), new Quaternion ());
			Instantiate (cube, new Vector3 (0, 0,  125f), new Quaternion ());

			startPlay = false;
		}

		if (rb.position.x > 17 || rb.position.x < -17) {
			isGameOver = true;
		}

		if (isGameOver) {
			gameOver.gameObject.SetActive (true);
			restartGame.gameObject.SetActive (true);
			finalScore.gameObject.SetActive(true);

			finalScore.text = "Score: " + score.number;

			speed = 0;
			globalSpeed = 0;
		} else {
			speed += 0.05f;
			globalSpeed = speed;
		}

	}

	int i = 0;
	int l = 0;
	float z = 125;


	int iWood = 0;
	float zWood = 100;
	// Update is called once per frame

	void Update(){
		if(!isGameOver){
			map ();
			wood ();
		}
	}

	void map () {
		i++;
		if (i == 5) {
			z += 62.5f;
			if (l == 20) {
				Instantiate (cube, new Vector3 (0, 0,  z), new Quaternion ());
				l = 10;
			} else {
				Instantiate (cube2, new Vector3 (0, 0, z), new Quaternion ());
				l = 20;
			}
			i = 0;
		}
	}

	void wood () {
		iWood++;
		if (iWood == 5) {
			zWood += 40f;

			var number = Random.Range(0,25);


			if (number < 5) {
				Instantiate (woodImage, new Vector3 (16, 0.5f, zWood), new Quaternion ());
			} else if (number > 15) {
				Instantiate (woodImage, new Vector3 (-15, 0.5f, zWood), new Quaternion ());
			} else if (number > 5 && number < 10) {
				Instantiate (boatImage, new Vector3 (7, 0.5f, zWood), new Quaternion ());
			} else if (number > 10 && number < 15) {
				Instantiate (boatImage, new Vector3 (-8, 0.5f, zWood), new Quaternion ());
			} else {
				Instantiate (woodImage, new Vector3 (0, 0.5f, zWood), new Quaternion ());
			}

			iWood = 0;
		}
	}

}	