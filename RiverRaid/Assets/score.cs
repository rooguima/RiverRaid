﻿using UnityEngine;
using System.Collections;

public class score : MonoBehaviour {

	private Rigidbody rb;

	public static int number = 0;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		number = 0;
	}

	// Update is called once per frame
	void Update () {
		if (!airplaneMover.isGameOver) {
			number += 1;
			GetComponent<TextMesh>().text = ""+number+"";


			Vector3 movement = new Vector3 (0, 0, 1);
			rb.velocity = movement * airplaneMover.globalSpeed;
		}
	}
}
