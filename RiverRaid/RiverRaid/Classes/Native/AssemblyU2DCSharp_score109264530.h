﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// score
struct  score_t109264530  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Rigidbody score::rb
	Rigidbody_t3346577219 * ___rb_2;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(score_t109264530, ___rb_2)); }
	inline Rigidbody_t3346577219 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody_t3346577219 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody_t3346577219 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}
};

struct score_t109264530_StaticFields
{
public:
	// System.Int32 score::number
	int32_t ___number_3;

public:
	inline static int32_t get_offset_of_number_3() { return static_cast<int32_t>(offsetof(score_t109264530_StaticFields, ___number_3)); }
	inline int32_t get_number_3() const { return ___number_3; }
	inline int32_t* get_address_of_number_3() { return &___number_3; }
	inline void set_number_3(int32_t value)
	{
		___number_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
