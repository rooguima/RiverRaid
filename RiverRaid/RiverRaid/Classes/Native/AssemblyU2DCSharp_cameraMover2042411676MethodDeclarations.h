﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraMover
struct cameraMover_t2042411676;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraMover::.ctor()
extern "C"  void cameraMover__ctor_m1172369487 (cameraMover_t2042411676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraMover::Start()
extern "C"  void cameraMover_Start_m119507279 (cameraMover_t2042411676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraMover::Update()
extern "C"  void cameraMover_Update_m3710577822 (cameraMover_t2042411676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
