﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// score
struct score_t109264530;

#include "codegen/il2cpp-codegen.h"

// System.Void score::.ctor()
extern "C"  void score__ctor_m3759853337 (score_t109264530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void score::.cctor()
extern "C"  void score__cctor_m109240244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void score::Start()
extern "C"  void score_Start_m2706991129 (score_t109264530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void score::Update()
extern "C"  void score_Update_m2318198548 (score_t109264530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
