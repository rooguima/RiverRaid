﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis4294105229.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepe99988271.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect3606982749.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy300513412.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV184977789.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec1643322606.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1922345195.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection1293548283.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility1171612705.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction94975348.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2627072750.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis3565360268.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState2895308594.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial639665897.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE1574154081.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit2757337633.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent2331340366.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1990156785.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1074114320.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping1257491342.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli1294793591.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter436718473.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As2149445162.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2777732396.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2493957633.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1153512176.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit1837657360.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1285073872.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fit909765868.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup169317941.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corne284493240.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1399125956.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons1640775616.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou1336501463.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical2052396382.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1596995480.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup352294875.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder1942933988.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility3144854024.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup423167365.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_DestroyByBoundary2738920731.h"
#include "AssemblyU2DCSharp_Boundary2244299850.h"
#include "AssemblyU2DCSharp_airplaneMover174355087.h"
#include "AssemblyU2DCSharp_cameraMover2042411676.h"
#include "AssemblyU2DCSharp_menu3347807.h"
#include "AssemblyU2DCSharp_score109264530.h"
#include "AssemblyU2DCSharp_wood3655341.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (Axis_t4294105229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1600[3] = 
{
	Axis_t4294105229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (U3CClickRepeatU3Ec__Iterator5_t99988271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[7] = 
{
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CaxisCoordinateU3E__1_2(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24PC_3(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24current_4(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU24U3EeventData_5(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (ScrollRect_t3606982749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[36] = 
{
	ScrollRect_t3606982749::get_offset_of_m_Content_2(),
	ScrollRect_t3606982749::get_offset_of_m_Horizontal_3(),
	ScrollRect_t3606982749::get_offset_of_m_Vertical_4(),
	ScrollRect_t3606982749::get_offset_of_m_MovementType_5(),
	ScrollRect_t3606982749::get_offset_of_m_Elasticity_6(),
	ScrollRect_t3606982749::get_offset_of_m_Inertia_7(),
	ScrollRect_t3606982749::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t3606982749::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t3606982749::get_offset_of_m_Viewport_10(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t3606982749::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t3606982749::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t3606982749::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t3606982749::get_offset_of_m_ViewRect_20(),
	ScrollRect_t3606982749::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t3606982749::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t3606982749::get_offset_of_m_Velocity_23(),
	ScrollRect_t3606982749::get_offset_of_m_Dragging_24(),
	ScrollRect_t3606982749::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t3606982749::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t3606982749::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t3606982749::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t3606982749::get_offset_of_m_Rect_33(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t3606982749::get_offset_of_m_Tracker_36(),
	ScrollRect_t3606982749::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (MovementType_t300513412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1603[4] = 
{
	MovementType_t300513412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (ScrollbarVisibility_t184977789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1604[4] = 
{
	ScrollbarVisibility_t184977789::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (ScrollRectEvent_t1643322606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (Selectable_t1885181538), -1, sizeof(Selectable_t1885181538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1606[14] = 
{
	Selectable_t1885181538_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1885181538::get_offset_of_m_Navigation_3(),
	Selectable_t1885181538::get_offset_of_m_Transition_4(),
	Selectable_t1885181538::get_offset_of_m_Colors_5(),
	Selectable_t1885181538::get_offset_of_m_SpriteState_6(),
	Selectable_t1885181538::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1885181538::get_offset_of_m_Interactable_8(),
	Selectable_t1885181538::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1885181538::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1885181538::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1885181538::get_offset_of_m_CanvasGroupCache_12(),
	Selectable_t1885181538::get_offset_of_U3CisPointerInsideU3Ek__BackingField_13(),
	Selectable_t1885181538::get_offset_of_U3CisPointerDownU3Ek__BackingField_14(),
	Selectable_t1885181538::get_offset_of_U3ChasSelectionU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Transition_t1922345195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1607[5] = 
{
	Transition_t1922345195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (SelectionState_t1293548283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[5] = 
{
	SelectionState_t1293548283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (SetPropertyUtility_t1171612705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (Slider_t79469677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[15] = 
{
	Slider_t79469677::get_offset_of_m_FillRect_16(),
	Slider_t79469677::get_offset_of_m_HandleRect_17(),
	Slider_t79469677::get_offset_of_m_Direction_18(),
	Slider_t79469677::get_offset_of_m_MinValue_19(),
	Slider_t79469677::get_offset_of_m_MaxValue_20(),
	Slider_t79469677::get_offset_of_m_WholeNumbers_21(),
	Slider_t79469677::get_offset_of_m_Value_22(),
	Slider_t79469677::get_offset_of_m_OnValueChanged_23(),
	Slider_t79469677::get_offset_of_m_FillImage_24(),
	Slider_t79469677::get_offset_of_m_FillTransform_25(),
	Slider_t79469677::get_offset_of_m_FillContainerRect_26(),
	Slider_t79469677::get_offset_of_m_HandleTransform_27(),
	Slider_t79469677::get_offset_of_m_HandleContainerRect_28(),
	Slider_t79469677::get_offset_of_m_Offset_29(),
	Slider_t79469677::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (Direction_t94975348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[5] = 
{
	Direction_t94975348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (SliderEvent_t2627072750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (Axis_t3565360268)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1613[3] = 
{
	Axis_t3565360268::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (SpriteState_t2895308594)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[3] = 
{
	SpriteState_t2895308594::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (StencilMaterial_t639665897), -1, sizeof(StencilMaterial_t639665897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	StencilMaterial_t639665897_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (MatEntry_t1574154081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[10] = 
{
	MatEntry_t1574154081::get_offset_of_baseMat_0(),
	MatEntry_t1574154081::get_offset_of_customMat_1(),
	MatEntry_t1574154081::get_offset_of_count_2(),
	MatEntry_t1574154081::get_offset_of_stencilId_3(),
	MatEntry_t1574154081::get_offset_of_operation_4(),
	MatEntry_t1574154081::get_offset_of_compareFunction_5(),
	MatEntry_t1574154081::get_offset_of_readMask_6(),
	MatEntry_t1574154081::get_offset_of_writeMask_7(),
	MatEntry_t1574154081::get_offset_of_useAlphaClip_8(),
	MatEntry_t1574154081::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Text_t9039225), -1, sizeof(Text_t9039225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1617[7] = 
{
	Text_t9039225::get_offset_of_m_FontData_28(),
	Text_t9039225::get_offset_of_m_Text_29(),
	Text_t9039225::get_offset_of_m_TextCache_30(),
	Text_t9039225::get_offset_of_m_TextCacheForLayout_31(),
	Text_t9039225_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t9039225::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t9039225::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (Toggle_t110812896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[5] = 
{
	Toggle_t110812896::get_offset_of_toggleTransition_16(),
	Toggle_t110812896::get_offset_of_graphic_17(),
	Toggle_t110812896::get_offset_of_m_Group_18(),
	Toggle_t110812896::get_offset_of_onValueChanged_19(),
	Toggle_t110812896::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (ToggleTransition_t2757337633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1619[3] = 
{
	ToggleTransition_t2757337633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (ToggleEvent_t2331340366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (ToggleGroup_t1990156785), -1, sizeof(ToggleGroup_t1990156785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1621[4] = 
{
	ToggleGroup_t1990156785::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1990156785::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (ClipperRegistry_t1074114320), -1, sizeof(ClipperRegistry_t1074114320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[2] = 
{
	ClipperRegistry_t1074114320_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1074114320::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (Clipping_t1257491342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (RectangularVertexClipper_t1294793591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[2] = 
{
	RectangularVertexClipper_t1294793591::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t1294793591::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (AspectRatioFitter_t436718473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[4] = 
{
	AspectRatioFitter_t436718473::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t436718473::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t436718473::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t436718473::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (AspectMode_t2149445162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1628[6] = 
{
	AspectMode_t2149445162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (CanvasScaler_t2777732396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[14] = 
{
	0,
	CanvasScaler_t2777732396::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2777732396::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2777732396::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2777732396::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2777732396::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2777732396::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2777732396::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2777732396::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2777732396::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (ScaleMode_t2493957633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1630[4] = 
{
	ScaleMode_t2493957633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (ScreenMatchMode_t1153512176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[4] = 
{
	ScreenMatchMode_t1153512176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (Unit_t1837657360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[6] = 
{
	Unit_t1837657360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (ContentSizeFitter_t1285073872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[4] = 
{
	ContentSizeFitter_t1285073872::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1285073872::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (FitMode_t909765868)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1634[4] = 
{
	FitMode_t909765868::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (GridLayoutGroup_t169317941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1635[6] = 
{
	GridLayoutGroup_t169317941::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t169317941::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t169317941::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t169317941::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t169317941::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t169317941::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (Corner_t284493240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1636[5] = 
{
	Corner_t284493240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (Axis_t1399125956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1637[3] = 
{
	Axis_t1399125956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (Constraint_t1640775616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[4] = 
{
	Constraint_t1640775616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (HorizontalLayoutGroup_t1336501463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (HorizontalOrVerticalLayoutGroup_t2052396382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[3] = 
{
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (LayoutElement_t1596995480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[7] = 
{
	LayoutElement_t1596995480::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1596995480::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1596995480::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (LayoutGroup_t352294875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[8] = 
{
	LayoutGroup_t352294875::get_offset_of_m_Padding_2(),
	LayoutGroup_t352294875::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t352294875::get_offset_of_m_Rect_4(),
	LayoutGroup_t352294875::get_offset_of_m_Tracker_5(),
	LayoutGroup_t352294875::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t352294875::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t352294875::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t352294875::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (LayoutRebuilder_t1942933988), -1, sizeof(LayoutRebuilder_t1942933988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1648[9] = 
{
	LayoutRebuilder_t1942933988::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t1942933988::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (LayoutUtility_t3144854024), -1, sizeof(LayoutUtility_t3144854024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[8] = 
{
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (VerticalLayoutGroup_t423167365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (DestroyByBoundary_t2738920731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (Boundary_t2244299850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[4] = 
{
	Boundary_t2244299850::get_offset_of_xMin_0(),
	Boundary_t2244299850::get_offset_of_xMax_1(),
	Boundary_t2244299850::get_offset_of_zMin_2(),
	Boundary_t2244299850::get_offset_of_zMax_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (airplaneMover_t174355087), -1, sizeof(airplaneMover_t174355087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1668[20] = 
{
	airplaneMover_t174355087::get_offset_of_rb_2(),
	airplaneMover_t174355087::get_offset_of_cube_3(),
	airplaneMover_t174355087::get_offset_of_cube2_4(),
	airplaneMover_t174355087::get_offset_of_woodImage_5(),
	airplaneMover_t174355087::get_offset_of_boatImage_6(),
	airplaneMover_t174355087::get_offset_of_shotSpawn_7(),
	airplaneMover_t174355087::get_offset_of_speed_8(),
	airplaneMover_t174355087::get_offset_of_tilt_9(),
	airplaneMover_t174355087_StaticFields::get_offset_of_globalSpeed_10(),
	airplaneMover_t174355087::get_offset_of_boundary_11(),
	airplaneMover_t174355087::get_offset_of_gameOver_12(),
	airplaneMover_t174355087::get_offset_of_finalScore_13(),
	airplaneMover_t174355087::get_offset_of_restartGame_14(),
	airplaneMover_t174355087_StaticFields::get_offset_of_isGameOver_15(),
	airplaneMover_t174355087::get_offset_of_startPlay_16(),
	airplaneMover_t174355087::get_offset_of_i_17(),
	airplaneMover_t174355087::get_offset_of_l_18(),
	airplaneMover_t174355087::get_offset_of_z_19(),
	airplaneMover_t174355087::get_offset_of_iWood_20(),
	airplaneMover_t174355087::get_offset_of_zWood_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (cameraMover_t2042411676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[3] = 
{
	cameraMover_t2042411676::get_offset_of_cameraDistOffset_2(),
	cameraMover_t2042411676::get_offset_of_mainCamera_3(),
	cameraMover_t2042411676::get_offset_of_player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (menu_t3347807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (score_t109264530), -1, sizeof(score_t109264530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	score_t109264530::get_offset_of_rb_2(),
	score_t109264530_StaticFields::get_offset_of_number_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (wood_t3655341), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
