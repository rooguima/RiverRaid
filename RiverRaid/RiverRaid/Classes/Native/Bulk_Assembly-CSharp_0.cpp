﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// airplaneMover
struct airplaneMover_t174355087;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// System.Object
struct Il2CppObject;
// Boundary
struct Boundary_t2244299850;
// cameraMover
struct cameraMover_t2042411676;
// UnityEngine.Camera
struct Camera_t2727095145;
// DestroyByBoundary
struct DestroyByBoundary_t2738920731;
// UnityEngine.Collider
struct Collider_t2939674232;
// menu
struct menu_t3347807;
// score
struct score_t109264530;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// wood
struct wood_t3655341;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_airplaneMover174355087.h"
#include "AssemblyU2DCSharp_airplaneMover174355087MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Touch4210255029MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_score109264530.h"
#include "AssemblyU2DCSharp_score109264530MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharp_Boundary2244299850.h"
#include "AssemblyU2DCSharp_Boundary2244299850MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharp_cameraMover2042411676.h"
#include "AssemblyU2DCSharp_cameraMover2042411676MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_DestroyByBoundary2738920731.h"
#include "AssemblyU2DCSharp_DestroyByBoundary2738920731MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "AssemblyU2DCSharp_menu3347807.h"
#include "AssemblyU2DCSharp_menu3347807MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "AssemblyU2DCSharp_wood3655341.h"
#include "AssemblyU2DCSharp_wood3655341MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, method) ((  Rigidbody_t3346577219 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t2567681854_m2237128948(__this, method) ((  TextMesh_t2567681854 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void airplaneMover::.ctor()
extern "C"  void airplaneMover__ctor_m1983668156 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	{
		__this->set_startPlay_16((bool)1);
		__this->set_z_19((125.0f));
		__this->set_zWood_21((100.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void airplaneMover::Start()
extern Il2CppClass* airplaneMover_t174355087_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var;
extern const uint32_t airplaneMover_Start_m930805948_MetadataUsageId;
extern "C"  void airplaneMover_Start_m930805948 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (airplaneMover_Start_m930805948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t3346577219 * L_0 = Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var);
		__this->set_rb_2(L_0);
		Text_t9039225 * L_1 = __this->get_gameOver_12();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		Text_t9039225 * L_3 = __this->get_finalScore_13();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		Button_t3896396478 * L_5 = __this->get_restartGame_14();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_isGameOver_15((bool)0);
		float L_7 = __this->get_speed_8();
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_globalSpeed_10(L_7);
		return;
	}
}
// System.Void airplaneMover::FixedUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* airplaneMover_t174355087_il2cpp_TypeInfo_var;
extern Il2CppClass* score_t109264530_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3588653336;
extern const uint32_t airplaneMover_FixedUpdate_m4166418487_MetadataUsageId;
extern "C"  void airplaneMover_FixedUpdate_m4166418487 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (airplaneMover_FixedUpdate_m4166418487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Touch_t4210255029  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Quaternion_t1553702882  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Quaternion_t1553702882  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Quaternion_t1553702882  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		V_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3635654872* L_1 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		V_1 = (*(Touch_t4210255029 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2_t4282066565  L_2 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = (&V_3)->get_x_1();
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)(((float)((float)((int32_t)((int32_t)L_4/(int32_t)2)))))))))
		{
			goto IL_0049;
		}
	}
	{
		V_0 = (-0.5f);
		goto IL_006c;
	}

IL_0049:
	{
		Vector2_t4282066565  L_5 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		V_4 = L_5;
		float L_6 = (&V_4)->get_x_1();
		int32_t L_7 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)(((float)((float)((int32_t)((int32_t)L_7/(int32_t)2)))))))))
		{
			goto IL_006c;
		}
	}
	{
		V_0 = (0.5f);
	}

IL_006c:
	{
		float L_8 = V_0;
		Vector3__ctor_m2926210380((&V_2), L_8, (0.0f), (1.0f), /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_9 = __this->get_rb_2();
		Vector3_t4282066566  L_10 = V_2;
		float L_11 = __this->get_speed_8();
		Vector3_t4282066566  L_12 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody_set_velocity_m799562119(L_9, L_12, /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_13 = __this->get_rb_2();
		Rigidbody_t3346577219 * L_14 = __this->get_rb_2();
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Rigidbody_get_velocity_m2696244068(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_x_1();
		float L_17 = __this->get_tilt_9();
		Quaternion_t1553702882  L_18 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), (0.0f), ((float)((float)L_16*(float)((-L_17)))), /*hidden argument*/NULL);
		NullCheck(L_13);
		Rigidbody_set_rotation_m3622714110(L_13, L_18, /*hidden argument*/NULL);
		bool L_19 = __this->get_startPlay_16();
		if (!L_19)
		{
			goto IL_015b;
		}
	}
	{
		Object_t3071478659 * L_20 = __this->get_cube_3();
		Vector3_t4282066566  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2926210380(&L_21, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_6));
		Quaternion_t1553702882  L_22 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		Object_t3071478659 * L_23 = __this->get_cube2_4();
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, (0.0f), (0.0f), (62.5f), /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_7));
		Quaternion_t1553702882  L_25 = V_7;
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		Object_t3071478659 * L_26 = __this->get_cube_3();
		Vector3_t4282066566  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2926210380(&L_27, (0.0f), (0.0f), (125.0f), /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_8));
		Quaternion_t1553702882  L_28 = V_8;
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		__this->set_startPlay_16((bool)0);
	}

IL_015b:
	{
		Rigidbody_t3346577219 * L_29 = __this->get_rb_2();
		NullCheck(L_29);
		Vector3_t4282066566  L_30 = Rigidbody_get_position_m1751901360(L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		float L_31 = (&V_9)->get_x_1();
		if ((((float)L_31) > ((float)(17.0f))))
		{
			goto IL_0197;
		}
	}
	{
		Rigidbody_t3346577219 * L_32 = __this->get_rb_2();
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Rigidbody_get_position_m1751901360(L_32, /*hidden argument*/NULL);
		V_10 = L_33;
		float L_34 = (&V_10)->get_x_1();
		if ((!(((float)L_34) < ((float)(-17.0f)))))
		{
			goto IL_019d;
		}
	}

IL_0197:
	{
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_isGameOver_15((bool)1);
	}

IL_019d:
	{
		bool L_35 = ((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->get_isGameOver_15();
		if (!L_35)
		{
			goto IL_0213;
		}
	}
	{
		Text_t9039225 * L_36 = __this->get_gameOver_12();
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = Component_get_gameObject_m1170635899(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		GameObject_SetActive_m3538205401(L_37, (bool)1, /*hidden argument*/NULL);
		Button_t3896396478 * L_38 = __this->get_restartGame_14();
		NullCheck(L_38);
		GameObject_t3674682005 * L_39 = Component_get_gameObject_m1170635899(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		GameObject_SetActive_m3538205401(L_39, (bool)1, /*hidden argument*/NULL);
		Text_t9039225 * L_40 = __this->get_finalScore_13();
		NullCheck(L_40);
		GameObject_t3674682005 * L_41 = Component_get_gameObject_m1170635899(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		GameObject_SetActive_m3538205401(L_41, (bool)1, /*hidden argument*/NULL);
		Text_t9039225 * L_42 = __this->get_finalScore_13();
		IL2CPP_RUNTIME_CLASS_INIT(score_t109264530_il2cpp_TypeInfo_var);
		int32_t L_43 = ((score_t109264530_StaticFields*)score_t109264530_il2cpp_TypeInfo_var->static_fields)->get_number_3();
		int32_t L_44 = L_43;
		Il2CppObject * L_45 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3588653336, L_45, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_46);
		__this->set_speed_8((0.0f));
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_globalSpeed_10((0.0f));
		goto IL_0230;
	}

IL_0213:
	{
		float L_47 = __this->get_speed_8();
		__this->set_speed_8(((float)((float)L_47+(float)(0.05f))));
		float L_48 = __this->get_speed_8();
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_globalSpeed_10(L_48);
	}

IL_0230:
	{
		return;
	}
}
// System.Void airplaneMover::Update()
extern Il2CppClass* airplaneMover_t174355087_il2cpp_TypeInfo_var;
extern const uint32_t airplaneMover_Update_m3091032785_MetadataUsageId;
extern "C"  void airplaneMover_Update_m3091032785 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (airplaneMover_Update_m3091032785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->get_isGameOver_15();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		airplaneMover_map_m4042298774(__this, /*hidden argument*/NULL);
		airplaneMover_wood_m1056459093(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void airplaneMover::map()
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t airplaneMover_map_m4042298774_MetadataUsageId;
extern "C"  void airplaneMover_map_m4042298774 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (airplaneMover_map_m4042298774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_i_17();
		__this->set_i_17(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_i_17();
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = __this->get_z_19();
		__this->set_z_19(((float)((float)L_2+(float)(62.5f))));
		int32_t L_3 = __this->get_l_18();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0070;
		}
	}
	{
		Object_t3071478659 * L_4 = __this->get_cube_3();
		float L_5 = __this->get_z_19();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, (0.0f), (0.0f), L_5, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t1553702882  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_4, L_6, L_7, /*hidden argument*/NULL);
		__this->set_l_18(((int32_t)10));
		goto IL_00a2;
	}

IL_0070:
	{
		Object_t3071478659 * L_8 = __this->get_cube2_4();
		float L_9 = __this->get_z_19();
		Vector3_t4282066566  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, (0.0f), (0.0f), L_9, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_1));
		Quaternion_t1553702882  L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_8, L_10, L_11, /*hidden argument*/NULL);
		__this->set_l_18(((int32_t)20));
	}

IL_00a2:
	{
		__this->set_i_17(0);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void airplaneMover::wood()
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t airplaneMover_wood_m1056459093_MetadataUsageId;
extern "C"  void airplaneMover_wood_m1056459093 (airplaneMover_t174355087 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (airplaneMover_wood_m1056459093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t1553702882  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t1553702882  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t1553702882  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Quaternion_t1553702882  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_iWood_20();
		__this->set_iWood_20(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_iWood_20();
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_0152;
		}
	}
	{
		float L_2 = __this->get_zWood_21();
		__this->set_zWood_21(((float)((float)L_2+(float)(40.0f))));
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)25), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)5)))
		{
			goto IL_006b;
		}
	}
	{
		Object_t3071478659 * L_5 = __this->get_woodImage_5();
		float L_6 = __this->get_zWood_21();
		Vector3_t4282066566  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2926210380(&L_7, (16.0f), (0.5f), L_6, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_1));
		Quaternion_t1553702882  L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_5, L_7, L_8, /*hidden argument*/NULL);
		goto IL_014b;
	}

IL_006b:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)15))))
		{
			goto IL_00a2;
		}
	}
	{
		Object_t3071478659 * L_10 = __this->get_woodImage_5();
		float L_11 = __this->get_zWood_21();
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, (-15.0f), (0.5f), L_11, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_2));
		Quaternion_t1553702882  L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_10, L_12, L_13, /*hidden argument*/NULL);
		goto IL_014b;
	}

IL_00a2:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) <= ((int32_t)5)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00e0;
		}
	}
	{
		Object_t3071478659 * L_16 = __this->get_boatImage_6();
		float L_17 = __this->get_zWood_21();
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, (7.0f), (0.5f), L_17, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_3));
		Quaternion_t1553702882  L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_16, L_18, L_19, /*hidden argument*/NULL);
		goto IL_014b;
	}

IL_00e0:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0120;
		}
	}
	{
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) >= ((int32_t)((int32_t)15))))
		{
			goto IL_0120;
		}
	}
	{
		Object_t3071478659 * L_22 = __this->get_boatImage_6();
		float L_23 = __this->get_zWood_21();
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, (-8.0f), (0.5f), L_23, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_4));
		Quaternion_t1553702882  L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_22, L_24, L_25, /*hidden argument*/NULL);
		goto IL_014b;
	}

IL_0120:
	{
		Object_t3071478659 * L_26 = __this->get_woodImage_5();
		float L_27 = __this->get_zWood_21();
		Vector3_t4282066566  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, (0.0f), (0.5f), L_27, /*hidden argument*/NULL);
		Initobj (Quaternion_t1553702882_il2cpp_TypeInfo_var, (&V_5));
		Quaternion_t1553702882  L_29 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_26, L_28, L_29, /*hidden argument*/NULL);
	}

IL_014b:
	{
		__this->set_iWood_20(0);
	}

IL_0152:
	{
		return;
	}
}
// System.Void Boundary::.ctor()
extern "C"  void Boundary__ctor_m2603328913 (Boundary_t2244299850 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void cameraMover::.ctor()
extern "C"  void cameraMover__ctor_m1172369487 (cameraMover_t2042411676 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void cameraMover::Start()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t cameraMover_Start_m119507279_MetadataUsageId;
extern "C"  void cameraMover_Start_m119507279 (cameraMover_t2042411676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (cameraMover_Start_m119507279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		__this->set_mainCamera_3(L_0);
		return;
	}
}
// System.Void cameraMover::Update()
extern "C"  void cameraMover_Update_m3710577822 (cameraMover_t2042411676 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t3674682005 * L_0 = __this->get_player_4();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Camera_t2727095145 * L_4 = __this->get_mainCamera_3();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_y_2();
		float L_7 = __this->get_cameraDistOffset_2();
		float L_8 = (&V_0)->get_z_3();
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (0.0f), ((float)((float)L_6+(float)L_7)), ((float)((float)L_8+(float)(54.0f))), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m3111394108(L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByBoundary::.ctor()
extern "C"  void DestroyByBoundary__ctor_m1790090928 (DestroyByBoundary_t2738920731 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByBoundary::OnTriggerExit(UnityEngine.Collider)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t DestroyByBoundary_OnTriggerExit_m1776189914_MetadataUsageId;
extern "C"  void DestroyByBoundary_OnTriggerExit_m1776189914 (DestroyByBoundary_t2738920731 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DestroyByBoundary_OnTriggerExit_m1776189914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void menu::.ctor()
extern "C"  void menu__ctor_m875892764 (menu_t3347807 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void menu::StartGame()
extern Il2CppCodeGenString* _stringLiteral71749459;
extern Il2CppCodeGenString* _stringLiteral2390489;
extern const uint32_t menu_StartGame_m2335188782_MetadataUsageId;
extern "C"  void menu_StartGame_m2335188782 (menu_t3347807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (menu_StartGame_m2335188782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral71749459, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2390489, /*hidden argument*/NULL);
		return;
	}
}
// System.Void score::.ctor()
extern "C"  void score__ctor_m3759853337 (score_t109264530 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void score::.cctor()
extern "C"  void score__cctor_m109240244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void score::Start()
extern Il2CppClass* score_t109264530_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var;
extern const uint32_t score_Start_m2706991129_MetadataUsageId;
extern "C"  void score_Start_m2706991129 (score_t109264530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (score_Start_m2706991129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t3346577219 * L_0 = Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var);
		__this->set_rb_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(score_t109264530_il2cpp_TypeInfo_var);
		((score_t109264530_StaticFields*)score_t109264530_il2cpp_TypeInfo_var->static_fields)->set_number_3(0);
		return;
	}
}
// System.Void score::Update()
extern Il2CppClass* airplaneMover_t174355087_il2cpp_TypeInfo_var;
extern Il2CppClass* score_t109264530_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t2567681854_m2237128948_MethodInfo_var;
extern const uint32_t score_Update_m2318198548_MetadataUsageId;
extern "C"  void score_Update_m2318198548 (score_t109264530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (score_Update_m2318198548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = ((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->get_isGameOver_15();
		if (L_0)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t109264530_il2cpp_TypeInfo_var);
		int32_t L_1 = ((score_t109264530_StaticFields*)score_t109264530_il2cpp_TypeInfo_var->static_fields)->get_number_3();
		((score_t109264530_StaticFields*)score_t109264530_il2cpp_TypeInfo_var->static_fields)->set_number_3(((int32_t)((int32_t)L_1+(int32_t)1)));
		TextMesh_t2567681854 * L_2 = Component_GetComponent_TisTextMesh_t2567681854_m2237128948(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t2567681854_m2237128948_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_4 = ((score_t109264530_StaticFields*)score_t109264530_il2cpp_TypeInfo_var->static_fields)->get_number_3();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_2);
		TextMesh_set_text_m3628430759(L_2, L_8, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_0), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_9 = __this->get_rb_2();
		Vector3_t4282066566  L_10 = V_0;
		float L_11 = ((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->get_globalSpeed_10();
		Vector3_t4282066566  L_12 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody_set_velocity_m799562119(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void wood::.ctor()
extern "C"  void wood__ctor_m776468110 (wood_t3655341 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void wood::Start()
extern "C"  void wood_Start_m4018573198 (wood_t3655341 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void wood::Update()
extern "C"  void wood_Update_m27569727 (wood_t3655341 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void wood::OnTriggerEnter()
extern Il2CppClass* airplaneMover_t174355087_il2cpp_TypeInfo_var;
extern const uint32_t wood_OnTriggerEnter_m3548527221_MetadataUsageId;
extern "C"  void wood_OnTriggerEnter_m3548527221 (wood_t3655341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (wood_OnTriggerEnter_m3548527221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((airplaneMover_t174355087_StaticFields*)airplaneMover_t174355087_il2cpp_TypeInfo_var->static_fields)->set_isGameOver_15((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
