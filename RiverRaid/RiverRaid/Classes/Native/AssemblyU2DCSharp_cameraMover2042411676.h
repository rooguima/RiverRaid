﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraMover
struct  cameraMover_t2042411676  : public MonoBehaviour_t667441552
{
public:
	// System.Single cameraMover::cameraDistOffset
	float ___cameraDistOffset_2;
	// UnityEngine.Camera cameraMover::mainCamera
	Camera_t2727095145 * ___mainCamera_3;
	// UnityEngine.GameObject cameraMover::player
	GameObject_t3674682005 * ___player_4;

public:
	inline static int32_t get_offset_of_cameraDistOffset_2() { return static_cast<int32_t>(offsetof(cameraMover_t2042411676, ___cameraDistOffset_2)); }
	inline float get_cameraDistOffset_2() const { return ___cameraDistOffset_2; }
	inline float* get_address_of_cameraDistOffset_2() { return &___cameraDistOffset_2; }
	inline void set_cameraDistOffset_2(float value)
	{
		___cameraDistOffset_2 = value;
	}

	inline static int32_t get_offset_of_mainCamera_3() { return static_cast<int32_t>(offsetof(cameraMover_t2042411676, ___mainCamera_3)); }
	inline Camera_t2727095145 * get_mainCamera_3() const { return ___mainCamera_3; }
	inline Camera_t2727095145 ** get_address_of_mainCamera_3() { return &___mainCamera_3; }
	inline void set_mainCamera_3(Camera_t2727095145 * value)
	{
		___mainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_3, value);
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(cameraMover_t2042411676, ___player_4)); }
	inline GameObject_t3674682005 * get_player_4() const { return ___player_4; }
	inline GameObject_t3674682005 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t3674682005 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier(&___player_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
