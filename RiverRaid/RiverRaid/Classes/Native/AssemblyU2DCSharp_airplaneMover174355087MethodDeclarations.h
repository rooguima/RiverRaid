﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// airplaneMover
struct airplaneMover_t174355087;

#include "codegen/il2cpp-codegen.h"

// System.Void airplaneMover::.ctor()
extern "C"  void airplaneMover__ctor_m1983668156 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void airplaneMover::Start()
extern "C"  void airplaneMover_Start_m930805948 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void airplaneMover::FixedUpdate()
extern "C"  void airplaneMover_FixedUpdate_m4166418487 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void airplaneMover::Update()
extern "C"  void airplaneMover_Update_m3091032785 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void airplaneMover::map()
extern "C"  void airplaneMover_map_m4042298774 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void airplaneMover::wood()
extern "C"  void airplaneMover_wood_m1056459093 (airplaneMover_t174355087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
