﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Transform
struct Transform_t1659122786;
// Boundary
struct Boundary_t2244299850;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// airplaneMover
struct  airplaneMover_t174355087  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Rigidbody airplaneMover::rb
	Rigidbody_t3346577219 * ___rb_2;
	// UnityEngine.Object airplaneMover::cube
	Object_t3071478659 * ___cube_3;
	// UnityEngine.Object airplaneMover::cube2
	Object_t3071478659 * ___cube2_4;
	// UnityEngine.Object airplaneMover::woodImage
	Object_t3071478659 * ___woodImage_5;
	// UnityEngine.Object airplaneMover::boatImage
	Object_t3071478659 * ___boatImage_6;
	// UnityEngine.Transform airplaneMover::shotSpawn
	Transform_t1659122786 * ___shotSpawn_7;
	// System.Single airplaneMover::speed
	float ___speed_8;
	// System.Single airplaneMover::tilt
	float ___tilt_9;
	// Boundary airplaneMover::boundary
	Boundary_t2244299850 * ___boundary_11;
	// UnityEngine.UI.Text airplaneMover::gameOver
	Text_t9039225 * ___gameOver_12;
	// UnityEngine.UI.Text airplaneMover::finalScore
	Text_t9039225 * ___finalScore_13;
	// UnityEngine.UI.Button airplaneMover::restartGame
	Button_t3896396478 * ___restartGame_14;
	// System.Boolean airplaneMover::startPlay
	bool ___startPlay_16;
	// System.Int32 airplaneMover::i
	int32_t ___i_17;
	// System.Int32 airplaneMover::l
	int32_t ___l_18;
	// System.Single airplaneMover::z
	float ___z_19;
	// System.Int32 airplaneMover::iWood
	int32_t ___iWood_20;
	// System.Single airplaneMover::zWood
	float ___zWood_21;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___rb_2)); }
	inline Rigidbody_t3346577219 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody_t3346577219 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody_t3346577219 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_cube_3() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___cube_3)); }
	inline Object_t3071478659 * get_cube_3() const { return ___cube_3; }
	inline Object_t3071478659 ** get_address_of_cube_3() { return &___cube_3; }
	inline void set_cube_3(Object_t3071478659 * value)
	{
		___cube_3 = value;
		Il2CppCodeGenWriteBarrier(&___cube_3, value);
	}

	inline static int32_t get_offset_of_cube2_4() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___cube2_4)); }
	inline Object_t3071478659 * get_cube2_4() const { return ___cube2_4; }
	inline Object_t3071478659 ** get_address_of_cube2_4() { return &___cube2_4; }
	inline void set_cube2_4(Object_t3071478659 * value)
	{
		___cube2_4 = value;
		Il2CppCodeGenWriteBarrier(&___cube2_4, value);
	}

	inline static int32_t get_offset_of_woodImage_5() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___woodImage_5)); }
	inline Object_t3071478659 * get_woodImage_5() const { return ___woodImage_5; }
	inline Object_t3071478659 ** get_address_of_woodImage_5() { return &___woodImage_5; }
	inline void set_woodImage_5(Object_t3071478659 * value)
	{
		___woodImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___woodImage_5, value);
	}

	inline static int32_t get_offset_of_boatImage_6() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___boatImage_6)); }
	inline Object_t3071478659 * get_boatImage_6() const { return ___boatImage_6; }
	inline Object_t3071478659 ** get_address_of_boatImage_6() { return &___boatImage_6; }
	inline void set_boatImage_6(Object_t3071478659 * value)
	{
		___boatImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___boatImage_6, value);
	}

	inline static int32_t get_offset_of_shotSpawn_7() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___shotSpawn_7)); }
	inline Transform_t1659122786 * get_shotSpawn_7() const { return ___shotSpawn_7; }
	inline Transform_t1659122786 ** get_address_of_shotSpawn_7() { return &___shotSpawn_7; }
	inline void set_shotSpawn_7(Transform_t1659122786 * value)
	{
		___shotSpawn_7 = value;
		Il2CppCodeGenWriteBarrier(&___shotSpawn_7, value);
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_tilt_9() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___tilt_9)); }
	inline float get_tilt_9() const { return ___tilt_9; }
	inline float* get_address_of_tilt_9() { return &___tilt_9; }
	inline void set_tilt_9(float value)
	{
		___tilt_9 = value;
	}

	inline static int32_t get_offset_of_boundary_11() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___boundary_11)); }
	inline Boundary_t2244299850 * get_boundary_11() const { return ___boundary_11; }
	inline Boundary_t2244299850 ** get_address_of_boundary_11() { return &___boundary_11; }
	inline void set_boundary_11(Boundary_t2244299850 * value)
	{
		___boundary_11 = value;
		Il2CppCodeGenWriteBarrier(&___boundary_11, value);
	}

	inline static int32_t get_offset_of_gameOver_12() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___gameOver_12)); }
	inline Text_t9039225 * get_gameOver_12() const { return ___gameOver_12; }
	inline Text_t9039225 ** get_address_of_gameOver_12() { return &___gameOver_12; }
	inline void set_gameOver_12(Text_t9039225 * value)
	{
		___gameOver_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameOver_12, value);
	}

	inline static int32_t get_offset_of_finalScore_13() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___finalScore_13)); }
	inline Text_t9039225 * get_finalScore_13() const { return ___finalScore_13; }
	inline Text_t9039225 ** get_address_of_finalScore_13() { return &___finalScore_13; }
	inline void set_finalScore_13(Text_t9039225 * value)
	{
		___finalScore_13 = value;
		Il2CppCodeGenWriteBarrier(&___finalScore_13, value);
	}

	inline static int32_t get_offset_of_restartGame_14() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___restartGame_14)); }
	inline Button_t3896396478 * get_restartGame_14() const { return ___restartGame_14; }
	inline Button_t3896396478 ** get_address_of_restartGame_14() { return &___restartGame_14; }
	inline void set_restartGame_14(Button_t3896396478 * value)
	{
		___restartGame_14 = value;
		Il2CppCodeGenWriteBarrier(&___restartGame_14, value);
	}

	inline static int32_t get_offset_of_startPlay_16() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___startPlay_16)); }
	inline bool get_startPlay_16() const { return ___startPlay_16; }
	inline bool* get_address_of_startPlay_16() { return &___startPlay_16; }
	inline void set_startPlay_16(bool value)
	{
		___startPlay_16 = value;
	}

	inline static int32_t get_offset_of_i_17() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___i_17)); }
	inline int32_t get_i_17() const { return ___i_17; }
	inline int32_t* get_address_of_i_17() { return &___i_17; }
	inline void set_i_17(int32_t value)
	{
		___i_17 = value;
	}

	inline static int32_t get_offset_of_l_18() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___l_18)); }
	inline int32_t get_l_18() const { return ___l_18; }
	inline int32_t* get_address_of_l_18() { return &___l_18; }
	inline void set_l_18(int32_t value)
	{
		___l_18 = value;
	}

	inline static int32_t get_offset_of_z_19() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___z_19)); }
	inline float get_z_19() const { return ___z_19; }
	inline float* get_address_of_z_19() { return &___z_19; }
	inline void set_z_19(float value)
	{
		___z_19 = value;
	}

	inline static int32_t get_offset_of_iWood_20() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___iWood_20)); }
	inline int32_t get_iWood_20() const { return ___iWood_20; }
	inline int32_t* get_address_of_iWood_20() { return &___iWood_20; }
	inline void set_iWood_20(int32_t value)
	{
		___iWood_20 = value;
	}

	inline static int32_t get_offset_of_zWood_21() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087, ___zWood_21)); }
	inline float get_zWood_21() const { return ___zWood_21; }
	inline float* get_address_of_zWood_21() { return &___zWood_21; }
	inline void set_zWood_21(float value)
	{
		___zWood_21 = value;
	}
};

struct airplaneMover_t174355087_StaticFields
{
public:
	// System.Single airplaneMover::globalSpeed
	float ___globalSpeed_10;
	// System.Boolean airplaneMover::isGameOver
	bool ___isGameOver_15;

public:
	inline static int32_t get_offset_of_globalSpeed_10() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087_StaticFields, ___globalSpeed_10)); }
	inline float get_globalSpeed_10() const { return ___globalSpeed_10; }
	inline float* get_address_of_globalSpeed_10() { return &___globalSpeed_10; }
	inline void set_globalSpeed_10(float value)
	{
		___globalSpeed_10 = value;
	}

	inline static int32_t get_offset_of_isGameOver_15() { return static_cast<int32_t>(offsetof(airplaneMover_t174355087_StaticFields, ___isGameOver_15)); }
	inline bool get_isGameOver_15() const { return ___isGameOver_15; }
	inline bool* get_address_of_isGameOver_15() { return &___isGameOver_15; }
	inline void set_isGameOver_15(bool value)
	{
		___isGameOver_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
