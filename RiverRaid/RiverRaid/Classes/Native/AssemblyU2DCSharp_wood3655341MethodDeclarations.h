﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// wood
struct wood_t3655341;

#include "codegen/il2cpp-codegen.h"

// System.Void wood::.ctor()
extern "C"  void wood__ctor_m776468110 (wood_t3655341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wood::Start()
extern "C"  void wood_Start_m4018573198 (wood_t3655341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wood::Update()
extern "C"  void wood_Update_m27569727 (wood_t3655341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void wood::OnTriggerEnter()
extern "C"  void wood_OnTriggerEnter_m3548527221 (wood_t3655341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
