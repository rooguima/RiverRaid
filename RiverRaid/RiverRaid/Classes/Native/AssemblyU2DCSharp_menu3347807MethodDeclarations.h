﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// menu
struct menu_t3347807;

#include "codegen/il2cpp-codegen.h"

// System.Void menu::.ctor()
extern "C"  void menu__ctor_m875892764 (menu_t3347807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void menu::StartGame()
extern "C"  void menu_StartGame_m2335188782 (menu_t3347807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
